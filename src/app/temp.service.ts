import { map,catchError } from 'rxjs/operators';
import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL="http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY="73603b79d09bec086edcd24422c1b248";
  private IMP = "&units=metric";

  constructor(private http:HttpClient) { }
searchWeatherData(cityName:String):Observable<Weather>{
  return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
  .pipe(map(data=>this.transformWeatherData(data)),
  catchError(this.handleerror)
  )
}
private transformWeatherData(data:WeatherRaw):Weather{
  return{
    name:data.name,
    country:data.sys.country,
    image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
    description:data.weather[0].description,
    temperature:data.main.temp,
    lat:data.coord.lat,
    lon:data.coord.lon,
  }
}
private handleerror(res:HttpErrorResponse){
  console.log(res.error);
  return throwError(res.error)
}

}
