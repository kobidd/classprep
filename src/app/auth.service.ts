import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
user:Observable<User | null>

  constructor(public afAuth:AngularFireAuth,
              private router:Router) { 
    this.user = this.afAuth.authState;
  }
signUp(email:string,password:string){
  this.afAuth.auth.createUserWithEmailAndPassword(email,password)
  .then(user=>{
    this.router.navigate(['/books'])
  })
}
logOut(){
  this.afAuth.auth.signOut().then(res=>console.log('sign out',res))
}
logIn(email:string,password:string){
  this.afAuth.auth.signInWithEmailAndPassword(email,password)
  .then(user=> {
    this.router.navigate(['/books'])
  })

}
}
