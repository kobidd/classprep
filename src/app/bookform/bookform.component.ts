import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConditionalExpr } from '@angular/compiler';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {
title:string
author:string
id:string
isEdit:Boolean=false
buttonText:string = "Add Book"
userId:string
  constructor(private booksservice:BooksService,
              private router:Router,
              private route:ActivatedRoute,
              private authservice:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authservice.user.subscribe(
      user=>{
        this.userId = user.uid;
        console.log(this.id)
    if(this.id){
      this.isEdit = true;
      this.buttonText = "Update Book"
      this.booksservice.getbook(this.id,this.userId).subscribe(
        book =>{
          this.title = book.data().title;
          this.author = book.data().author;
        }
      )
    }
      }
    )
    
  }
  onSubmit(){
    if(this.isEdit){
      this.booksservice.updatebook(this.userId,this.id,this.title,this.author)
    }
    else{
    this.booksservice.addBooks(this.userId,this.title,this.author)}
    //this.booksservice.addBooks(this.title,this.author)
    this.router.navigate(['/books']);
  }

}
