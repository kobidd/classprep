import { Weather } from './../interfaces/weather';
import { Observable } from 'rxjs';
import { TempService } from './../temp.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  constructor(private activatedroute:ActivatedRoute,
              private tempservice:TempService) { }
city:string;
temp:number;
tempData$:Observable<Weather>;
image:String;
errorMessage:String;
hasError:Boolean = false; 


likes:number=0;

addlikes(){
this.likes++;
}
  ngOnInit() {
   this.city = this.activatedroute.snapshot.params['city'];
   //this.temp = this.activatedroute.snapshot.params['temp'];
   this.tempData$ = this.tempservice.searchWeatherData(this.city);
   this.tempData$.subscribe(
     data=>{console.log(data);
      this.image = data.image;
      this.temp = data.temperature;},
      error=>{
        this.hasError = true;
        this.errorMessage = error.message;
        console.log("in the component " + error.message);
        console.log(this.errorMessage);
      }
   )

    }
  }