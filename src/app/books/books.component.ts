import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books$:Observable<any>;
  userId:string
  constructor(private booksservice:BooksService,
              public authservice:AuthService) { }

  ngOnInit() {
   this.authservice.user.subscribe(
    user =>{
      this.userId = user.uid
      this.books$ = this.booksservice.getbooks(this.userId)
    }) 
  }
  deleteBook(id:string){
    this.booksservice.deletebook(this.userId,id);
  }
}
