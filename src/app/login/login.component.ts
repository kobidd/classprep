import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
email:string
password:string
  constructor(public authservice:AuthService,
              private router:Router,
              private route:ActivatedRoute) { }

  ngOnInit() {
  }
  onSubmit(){
    this.authservice.logIn(this.email,this.password);
  }

}
