import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email:string
  password:string
  
  constructor(public authservice:AuthService,
              private router:Router) { }

  ngOnInit() {
  }
onSubmit(){
this.authservice.signUp(this.email,this.password)
this.router.navigate(['books']);
}
}
