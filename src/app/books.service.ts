import { Observable, observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  bookCollection:AngularFirestoreCollection;

books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'},{title:'Michal', author:'Amos Oz'}] 
getbooks(userId:string):Observable<any[]>{
  /*const booksObservable = new Observable(
    observer=>{
      setInterval(() => observer.next(this.books),5000)
    }
  )
  return booksObservable*/
//return this.db.collection('books').valueChanges({idField:'id'});
this.bookCollection = this.db.collection(`users/${userId}/books`);
return this.bookCollection.snapshotChanges().pipe(
  map(
    collection => collection.map(
      document=>{
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data
      }
    )
  )
)
  }

  addBooks(userId:string,title:string,author:string){
    const book = {title:title,author:author}
    //this.db.collection('books').add(book);
    this.userCollection.doc(userId).collection('books').add(book);
  }
  deletebook(userId:string,id:string){
    this.db.doc(`users/${userId}/books/${id}`).delete();
  }
  updatebook(userId:string,id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update({
      title:title,
      author:author
    }
    )
  }
  getbook(id:string,userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get()
  }
 
  constructor(private db:AngularFirestore) { }
}
