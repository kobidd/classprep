import {  Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  constructor(private router:Router) { }

  cityOp:object[] = [{name:'Jerusalem'}, {name:'Paris'},{name:'London'},{name:'Non existent'}]
  cityInput:string;
  tempInput:number;

  ngOnInit() {
  }

  onSubmit(){
    this.router.navigate(['/temperatures',this.cityInput,this.tempInput])
  }

}
