// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig:{
    apiKey: "AIzaSyCfgfvHpmpIxPod59VVXnY01nxHa7vlLyM",
    authDomain: "classprep-6d790.firebaseapp.com",
    databaseURL: "https://classprep-6d790.firebaseio.com",
    projectId: "classprep-6d790",
    storageBucket: "classprep-6d790.appspot.com",
    messagingSenderId: "1047518388086",
    appId: "1:1047518388086:web:76861e6b4d73b06e06a457",
    measurementId: "G-6QGS7KRSTP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
